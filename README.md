# REXdb #

REXdb: a reference database of transposable element protein domains
Database is described in the article:  Systematic survey of plant LTR-retrotransposons elucidates phylogenetic relationships of their polyprotein domains and provides a reference for element classification, Mobile DNA 2019, 10:1 https://doi.org/10.1186/s13100-018-0144-1

REXdb is utilized in repeat analysis tools RepeatExplorer2 and DANTE which are available on our Galaxy server(https://repeatexplorer-elixir.cerit-sc.cz)

REXdb is supported by ELIXIR-CZ

[![](https://www.elixir-czech.cz/@@site-logo/ELIXIR_CZECHREPUBLIC_white_background_small.png)](https://www.elixir-czech.cz)


